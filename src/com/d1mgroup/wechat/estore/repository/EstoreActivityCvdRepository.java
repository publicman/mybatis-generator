package com.d1mgroup.wechat.estore.repository;

import com.d1mgroup.wechat.estore.domain.entity.EstoreActivityCvd;
import com.d1mgroup.wechat.estore.domain.entity.EstoreActivityCvdExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EstoreActivityCvdRepository {
    long countByExample(EstoreActivityCvdExample example);

    int deleteByExample(EstoreActivityCvdExample example);

    int deleteByPrimaryKey(Long id);

    int insert(EstoreActivityCvd record);

    int insertSelective(EstoreActivityCvd record);

    List<EstoreActivityCvd> selectByExample(EstoreActivityCvdExample example);

    EstoreActivityCvd selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") EstoreActivityCvd record, @Param("example") EstoreActivityCvdExample example);

    int updateByExample(@Param("record") EstoreActivityCvd record, @Param("example") EstoreActivityCvdExample example);

    int updateByPrimaryKeySelective(EstoreActivityCvd record);

    int updateByPrimaryKey(EstoreActivityCvd record);

    int batchUpdateByKeys(@Param("recordList") List<EstoreActivityCvd> recordList);

    void batchInsert(List<EstoreActivityCvd> recordLst);

    int batchDeleteByKeys(@Param("ids") Long[] ids);
}