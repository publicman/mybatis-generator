package com.d1mgroup.wechat.estore.domain.entity;

import java.io.Serializable;
import java.util.Date;

public class EstoreActivityCvd implements Serializable {
    private Long id;

    private Integer wechatId;

    /**
     * 活动code
     */
    private String code;

    /**
     * 活动名称
     */
    private String title;

    /**
     * 描述
     */
    private String remark;

    /**
     * 心形图URL
     */
    private String heartUrl;

    /**
     * 香水下标(从0开始)
     */
    private Integer index;

    /**
     * 百分比
     */
    private String ratio;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 标签
     */
    private String tag;

    /**
     * 1:删除
     */
    private Integer isDelete;

    private Date createAt;

    private Date updateAt;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getWechatId() {
        return wechatId;
    }

    public void setWechatId(Integer wechatId) {
        this.wechatId = wechatId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getHeartUrl() {
        return heartUrl;
    }

    public void setHeartUrl(String heartUrl) {
        this.heartUrl = heartUrl == null ? null : heartUrl.trim();
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio == null ? null : ratio.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", wechatId=").append(wechatId);
        sb.append(", code=").append(code);
        sb.append(", title=").append(title);
        sb.append(", remark=").append(remark);
        sb.append(", heartUrl=").append(heartUrl);
        sb.append(", index=").append(index);
        sb.append(", ratio=").append(ratio);
        sb.append(", status=").append(status);
        sb.append(", tag=").append(tag);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", createAt=").append(createAt);
        sb.append(", updateAt=").append(updateAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}