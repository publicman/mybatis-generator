package org.mybatis.generator.plugins.comment;

import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 防止XML追加生成
 */
public class OverwriteXmlPlugin extends  PluginAdapter {
    @Override
     public boolean validate(List warnings) {
        return true;
    }
    @Override
     public boolean sqlMapGenerated(GeneratedXmlFile sqlMap, IntrospectedTable introspectedTable) {
        try {
            Field field = sqlMap.getClass().getDeclaredField("isMergeable");
            field.setAccessible(true);
            field.setBoolean(sqlMap,false);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}

